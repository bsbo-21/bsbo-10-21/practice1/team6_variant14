using System;
using System.Collections.Generic;

class Program {

    public static void Main()
    {
        Monopoly.getInstance().move();
    }
    
    public class User
    {
    	public Square position;
    }
    
    public class Monopoly
    {
        private static Monopoly shared;
        public Board board;
        public Dice dice;

        private Monopoly()
        {
        	board = new Board();
          	dice = new Dice();
        }

        public static Monopoly getInstance()
        {
        	if (shared == null)
        	{
        		shared = new Monopoly();
        	}
        	return shared;
        }

        public void move()
        {
        	Console.WriteLine("Метод move вызван у класса Monopoly");
        	Player somePlayer = new Player();
            somePlayer.getPosition();
            dice.roll();
            int total = dice.getTotal();
            Square someSquare = new Square();
            board.getSpace(someSquare, total);

        }
    }
    
    public class Dice 
    {
    	int valueDice1 = 0;
    	int valueDice2 = 0;
    	Random random = new Random();

    
    	public void roll()
    	{
    		Console.WriteLine("Метод roll у Dice");
    		valueDice1 = random.Next(1, 6 + 1);
    		valueDice2 = random.Next(1, 6 + 1);
    	}

    	public int getTotal()
    	{
    		Console.WriteLine("Метод getTotal вернул total (сумму значения двух кубиков) у класса Dice");
    		return valueDice1 + valueDice2;
    	}

    	public bool checkDouble() 
    	{
            Console.WriteLine("Вызван метод checkDouble у класса Dice");
    		return valueDice1 == valueDice2;
    	}
    }
    public class Board 
    {
    	public List<Square> squares;

    	public Board()
    	{
    		for (int i = 0; i < 64; i++)
    		{
          		squares = new List<Square>();
    			squares.Add(new Square());
    		}
    	}

    	public void getSpace(Square oldPos, int total)
        {
            Console.WriteLine("Метод getSpace был вызван у класса Board");
        }
    }
    public class Square {}
    public class Player 
    {

    	public void getPosition()
        {
            Console.WriteLine("Метод getPosition вернул oldPos у класса Player");
        }

    }
}
